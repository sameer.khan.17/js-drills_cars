function problem2(inventory)
{
    if(inventory==undefined || inventory.length==0)
    {
        return ([]);
    }
    else
    {
        const result=inventory.pop();
        const last_car_make=result.car_make;
        const last_car_model=result.car_model;
        console.log("Last car is a "+ last_car_make+ " " +            last_car_model );
    }

}
module.exports=problem2;
