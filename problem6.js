function problem6(inventory)
{
    if(inventory==undefined || inventory.length==0 )
    {
        return ([]);
    }
    else
    {
        let reqCars=[];
        for(let i=0;i<inventory.length;i++)
        {
            if(inventory[i]["car_make"]== "BMW" || inventory[i]["car_make"]== "Audi" )
            {
                reqCars.push(inventory[i]);

            }
        }
        return reqCars;
    }
}
module.exports=problem6;
