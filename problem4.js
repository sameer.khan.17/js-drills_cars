function problem4(inventory)
{
    let carYears=[];
    if(inventory==undefined || inventory.length==0)
    {
        return ([]);
    }
    else
    {
        for(let i=0;i<inventory.length;i++)
        {
            carYears.push(inventory[i].car_year);
        }
    }

    return carYears;

}

module.exports=problem4;
