function problem3(inventory)
{
    let car_models=[];
    if(inventory==undefined || inventory.length==0)
    {
        return ([]);
    }
    else
    {

        for(let i=0;i<inventory.length;i++)
        {
            car_models.push(inventory[i].car_model);

        }
    }

    car_models.sort();
    return car_models;

}
module.exports=problem3;

