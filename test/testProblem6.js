const inventory=require('../inventory.js');
const problem6=require('../problem6.js');

const result=problem6(inventory);

if (result.length == 0)
{
    console.log("Empty array is returned");
}
else
{
    console.log(JSON.stringify(result));
}

