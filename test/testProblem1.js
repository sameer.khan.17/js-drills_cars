const inventory=require('../inventory.js');
const problem1=require('../problem1.js');

const prompt = require('prompt-sync')();
const search_id = prompt('What is the car id that you want to search? ');

const result=problem1(inventory,search_id);
if (result.length == 0)
{
    console.log("Empty array is returned");
}
else
{
    console.log("Car 33 is a "+ result.car_year+ " " + result.car_make+ " " + result.car_model);
}

