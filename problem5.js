function problem5(inventory,carYears)
{
    if(inventory==undefined || inventory.length==0 ||  carYears==undefined || carYears.length==0 )
    {
        return ([]);
    }
    else
    {
        let oldCars=[];
        for(let i=0;i<carYears.length;i++)
        {
            if(carYears[i]<2000)
            {
                oldCars.push(carYears[i]);
            }

        }
        return oldCars;
    }
    

}
module.exports=problem5;

